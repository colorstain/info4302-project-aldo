# INFO4302 - Final 
Darlin Alberto, Aldo Garcia, Richard Dansoh
___________________________________________

To run our the CPI Visualizer, navigate to:
 src > dj_bls_visualizer

and run: python manage.py runserver

Finally, open your browser at: localhost:8000 

Once the server is running, here are example curl requests:

Returns Series Data:
curl -L http://localhost:8000/api/v1/series/?format=xml
curl -L http://localhost:8000/api/v1/series/?format=json

Returns Area Data for Specific Area:
curl -L http://localhost:8000/api/v1/area/A400/?format=xml
curl -L http://localhost:8000/api/v1/area/A400/?format=json

Returns Data on Transportation Item:
curl -L http://localhost:8000/api/v1/item/SAT/?format=xml
curl -L http://localhost:8000/api/v1/item/SAT/?format=json

Returns Observation Data:
curl -L http://localhost:8000/api/v1/observation/?format=xml
curl -L http://localhost:8000/api/v1/observation/?format=json

NOTE:

Though we forgot to show it in our presentation, we used Schema.org
to mark up our about page as can be seen by viewing the page source.

-----------------------

Requirements:

python 2.7.3
Django==1.4.2
django-treebeard==1.61
wsgiref==0.1.2
gviz_api==1.82