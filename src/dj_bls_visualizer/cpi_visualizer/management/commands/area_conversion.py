
"""
Parse through area data to retrieve tree hierarchy
"""

from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from cpi_visualizer.models import *
from django.conf import settings
import os
from django.db.utils import IntegrityError

def writeNewFile(data_file, formatted_file):
    """ creates a new data file we can process """

    # Read data file
    f = open(data_file, "r")
    rawData = f.readlines()
    f.close()

    #Begin writing new file
    new_file= open(formatted_file, "w")
    m=0

    for line in rawData:
        if m ==0:
            m+=1
        else:
            data = line.split()
            new_line = data[0][0:4] + "|" + data[0][4:]
            for iter in data[1:]:
                bool = iter != '-' and iter != 'A' and iter != 'B'
                bool2 = iter != 'C' and iter != 'D'
                bool3 = bool and bool2
                if len(iter) == 2 and bool3:
                    try:
                        int(iter)
                        new_line = new_line + "|" + iter
                    except ValueError:
                        new_line = new_line + iter
                elif len(iter) == 1 and bool3:
                    new_line = new_line + "|" + iter
                else: new_line = new_line + " " + iter
            new_file.write(new_line + '\n')
    new_file.close()



def createHierarchy(data_file):
    """reads a csv file linking movie IDs with actor IDs"""
    f = open(data_file,"r")
    rawData = f.readlines()
    f.close()

    m=0
    list = []
    for line in rawData:
        dict = {}   
        data = line.split("|")
        dict['area_code'] = data[0]
        dict['area'] = data[1]
        dict['display_level'] = int(data[2])
        dict['sequence'] = int(data[4].strip())
        list.append(dict)
    return list

def merge(left, right):
    result = []
    i,j = 0, 0
    while i < len(left) and j < len(right):
        if left[i]['sequence'] <= right[j]['sequence']:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
    result += left[i:]
    result += right[j:]
    return result

def mergesort(lst):
    if len(lst) <= 1:
        return lst
    middle = int(len(lst)/2)
    left = mergesort(lst[:middle])
    right = mergesort(lst[middle:])
    return merge(left, right)


def processData(table):
    tree = {}
    parents = []
    for entry in table:
        code = entry['area_code']
        tree[code] = {}
        tree[code]['area'] = entry['area']
        tree[code]['area_code'] = entry['area_code']

        if entry['display_level'] == 0:
            tree[code]['children'] = []
            tree[code]['parent'] = None
            for iter in table:
                num = entry['sequence']
                bool = iter['sequence'] > num and iter['display_level'] == 1
                if bool:
                    tree[code]['children'].append(iter['area_code'])
                elif iter['sequence'] > num and iter['display_level'] == 0:
                    break
            parents.append(tree[code])
        else:
            tree[code]['children'] = []

    for entry in table:
        code = entry['area_code']
        if entry['display_level'] == 1:
            for parent in parents:
                if entry['area_code'] in parent['children']:
                    tree[code]['parent'] = parent['area_code']
    return tree




class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--raw_file',
            action='store',
            dest='raw_file',
            help='the file with raw BLS area data, relative to the \'data\' directory',
            default='cu.area',
        ),
        make_option('--formatted_file',
            action='store',
            dest='formatted_file',
            help='the name of the resulting formatted file, relative to the \'data\' directory',
            default='formatted_area.txt'
        ),
    )

    def construct_subtree(self, tree, id):

        name = tree[id]['area'].strip()

        return {'data' : {'id': id, 'name': name}, 'children': [self.construct_subtree(tree, child_id) for child_id in tree[id]['children']]}

    def handle(self, *args, **options):
        if not options['raw_file'] or not options['formatted_file']:
            raise CommandError('raw_file and formatted_file are required arguments.')

        data_file = os.path.join(settings.DATA_DIR  , options['raw_file'])
        formatted_file = os.path.join(settings.DATA_DIR  , options['formatted_file'])

        try:
            writeNewFile(data_file, formatted_file)
        except IOError:
            raise CommandError('could not open raw area file')

        try:
            table = createHierarchy(formatted_file)
        except IOError:
            raise CommandError('could not open formatted area file')

        sorted_data = mergesort(table)
        tree = processData(sorted_data)

        for area_id, information in tree.items():
            area_name = information['area'].strip()
            parent_id = information['parent']
            parent_name = tree[information['parent']]['area'].strip() if information['parent'] else None


            if parent_id is None:
                area = Area.add_root(id=area_id, name=area_name)
            else:
                try:
                    parent, created = Area.objects.get_or_create(id=parent_id, name=parent_name)
                except IntegrityError:
                    parent = Area.add_root(id=parent_id, name=parent_name)

                area = parent.add_child(id=area_id, name=area_name)

            for child_id in information['children']:
                child_name = tree[child_id]['area'].strip()
                area.add_child(id=child_id, name=child_name)

        for area in Area.objects.all():
            print area.name
            print area.get_parent()