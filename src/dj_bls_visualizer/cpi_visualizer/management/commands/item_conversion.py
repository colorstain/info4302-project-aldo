"""
Parse through item data to retrieve tree hierarchy
Note: Need to edit out area code
"""

from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from cpi_visualizer.models import *
from django.conf import settings
import os
from collections import OrderedDict


def writeNewFile(data_file, formatted_file):
    """ creates a new data file we can process """
    
    # Read data file
    f = open(data_file, "r")
    rawData = f.readlines()
    f.close()
    
    #Begin writing new file
    new_file= open(formatted_file, "w")
    m=0
    
    for line in rawData:
        if m ==0:
            m+=1
        else:
            data = line.split()
            new_line = data[0] + "|"
            for iter in data[1:]:
                try:
                    int(iter)
                    new_line = new_line + "|" + iter
                except ValueError:
                    if iter == "T":
                        new_line = new_line + "|" + iter
                    else:
                        new_line = new_line + " " + iter  
            new_file.write(new_line + '\n')
    new_file.close()
    
    
    
def createHierarchy(data_file):
    """reads a csv file linking movie IDs with actor IDs"""
    f = open(data_file,"r")
    rawData = f.readlines()
    f.close()
    
    list = []
    for line in rawData:
        dict = OrderedDict()
        data = line.split("|")
        dict['item_code'] = data[0]
        dict['item_name'] = data[1].strip()
        dict['display_level'] = int(data[2])
        dict['sequence'] = int(data[4].strip())
        list.append(dict)
    return list
    
def merge(left, right):
    result = []
    i,j = 0, 0
    while i < len(left) and j < len(right):
        if left[i]['sequence'] <= right[j]['sequence']:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
    result += left[i:]
    result += right[j:]
    return result
        
def mergesort(lst):
    if len(lst) <= 1:
        return lst
    middle = int(len(lst)/2)
    left = mergesort(lst[:middle])
    right = mergesort(lst[middle:])
    return merge(left, right)
    
    
def processData(table):
    tree = OrderedDict()
    parents = OrderedDict()
    for entry in table:
        code = entry['item_code']
        tree[code] = {}
        tree[code]['item_name'] = entry['item_name']
        level = entry['display_level']
        tree[code]['children'] = []
        tree[code]['item_code'] = code
        num = entry['sequence']
        
        if level == 0:
            tree[code]['parent'] = None
            
        for iter in table:
            
            #child condition
            bool = iter['display_level'] == (level+1) and iter['sequence'] > num
            
            #break condition
            bool2 = iter['sequence'] > num and iter['display_level'] == level
            
            if bool:
                tree[code]['children'].append(iter['item_code'])
            elif bool2:
                break
            
        if parents.has_key(str(level)):
            parents[str(level)].append(tree[code])
        else:
            parents[str(level)] = []
            parents[str(level)].append(tree[code])
    
    for entry in table:
        code = entry['item_code']
        level = entry['display_level']
        if level != 0:
            for parent in parents[str(level-1)]:
                if entry['item_code'] in parent['children']:
                    tree[code]['parent'] = parent['item_code']
    return tree

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--raw_file',
            action='store',
            dest='raw_file',
            help='the file with raw BLS item data, relative to the \'data\' directory',
            default='cu.item',
        ),
        make_option('--formatted_file',
            action='store',
            dest='formatted_file',
            help='the name of the resulting formatted file, relative to the \'data\' directory',
            default='formatted_item.txt'
        ),
    )

    def handle(self, *args, **options):
        if not options['raw_file'] or not options['formatted_file']:
            raise CommandError('raw_file and formatted_file are required arguments.')

        data_file = os.path.join(settings.DATA_DIR  , options['raw_file'])
        formatted_file = os.path.join(settings.DATA_DIR  , options['formatted_file'])

        try:
            writeNewFile(data_file, formatted_file)
        except IOError:
            raise CommandError('could not open raw area file')

        try:
            table = createHierarchy(formatted_file)
        except IOError:
            raise CommandError('could not open formatted area file')

        sorted_data = mergesort(table)
        tree = processData(sorted_data)

        for item_id, information in tree.items():
            item_name = information['item_name'].strip()
            parent_id = information['parent']
            parent_name = tree[information['parent']]['item_name'].strip() if information['parent'] else None

            if parent_id is None:
                item = Item.add_root(id=item_id, name=item_name)
            else:
                parent = Item.objects.get(id=parent_id)
                item = parent.add_child(id=item_id, name=item_name)

            for child_id in information['children']:
                child_name = tree[child_id]['item_name'].strip()
                item.add_child(id=child_id, name=child_name)
