'''
Created on Nov 11, 2012

@author: aldo
'''

from django.core.management.base import BaseCommand
from django.conf import settings
import os
from cpi_visualizer.models import Item, Area, Series, Observation


class Command(BaseCommand):
    def handle(self, *args, **options):
        areas = self.read_from_file(os.path.join(settings.DATA_DIR, 'cu.area'))
        items = self.read_from_file(os.path.join(settings.DATA_DIR, 'cu.item'))
        all_series = self.read_from_file(os.path.join(settings.DATA_DIR, 'cu.series'))
        observations = self.read_from_file(os.path.join(settings.DATA_DIR, 'cu.data.0.Current'))
        self.save_to_db(areas, items, all_series, observations)

    def read_from_file(self, fileName):
        data = []
        #data = {}
        with open(fileName, 'r') as f:
            header = None
            for i, line in enumerate(f):
                line_split = [x.strip() for x in line.split("\t")]
                #print line_split
                if i == 0:
                    #print line
                    #print line_split
                    header = line_split
                else:
                    entry = {}
                    for i, datum in enumerate(line_split):
                        if datum != '':
                            entry[header[i]] = datum
                    data.append(entry)
        return data

    def save_to_db(self, areas, items, all_series, observations):

        for series in all_series:
            series_id = series['series_id']
            area_code = series['area_code']
            try:
                area = Area.objects.get(id=area_code)
            except Area.DoesNotExist:
                print 'Area', area_code, 'does not exist'
                raise
            item_code = series['item_code']
            try:
                item = Item.objects.get(id=item_code)
            except Item.DoesNotExist:
                print 'Item', item_code, 'does not exist'
                raise
            
            seasonal_code = series['seasonal']
            
            periodicity_code = series['periodicity_code']
            
            base_period = series['base_period']
            base_date = self.base_to_date(base_period)
            
            #TODO parse period
            begin_year = series['begin_year']
            begin_period = series['begin_period']
            begin_date = begin_year + '-' + self.to_month(begin_period)
            
            end_year = series['end_year']
            end_period = series['end_period']
            end_date = end_year + '-' + self.to_month(end_period)
            
            #TODO: survey type hardcoded for now
            s = Series(id=series_id, area=area,
                       item=item, seasonality=seasonal_code,
                       periodicity=periodicity_code, base_period=base_date,
                       begin_year=begin_date, end_year=end_date,
                       survey_type='CU')
            s.save()
            
        for observation in observations:
            ser_id = observation['series_id']
            try:
                ser = Series.objects.get(id=ser_id)
            except Series.DoesNotExist:
                print 'Series', ser_id, 'does not exist'
            
            
            year = observation['year']
            period = observation['period']
            date = year + '-' + self.to_month(period)
            
            value = observation['value']
            
            o = Observation(series = ser, date = date, value = value)
            o.save()

    def to_month(self, period):
            if period == 'M13' or period == 'S03':
                return '12-31'
            elif period == 'S01':
                return '01-15'
            elif period == 'S02':
                return '06-15'
            elif period[0] == 'M' and int(period[1:3]) < 13:
                return period[1:3]+'-01'
            
    def base_to_date(self, base_period):
        if base_period[0:3] == 'DEC': 
            #base period in format DECEMBER YYYY=100
            return base_period[9:13] + '-12-01'
        elif base_period[0:3] == 'NOV':
            #base period in format NOVEMBER YYYY=100
            return base_period[9:13] + '-11-01'
        elif base_period[0:3] == 'OCT':
            #base period in format OCTOBER YYYY=100
            return base_period[8:12] +'-10-01'
        elif base_period == '1982-84=100':
            return '1984-12-01'
        else:
            #base period in format YYYY=100
            year = base_period.split('=')[0]
            return year + '-01-01'

    
if __name__ == '__main__':
    pass
