# Create your views here.

from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotFound
from django.shortcuts import render_to_response
from django.utils import simplejson
from cpi_visualizer.models import Observation, Series
import gviz_api
import datetime



def home(request):
    years = range(1980, datetime.date.today().year + 1)
    years.reverse()
    return render_to_response('main.html',{
                                            'title': "CPI Visualizer",
                                            'years': years,
                                            })
    
def api(request):
    return render_to_response('api.html',{
                                            'title': "CPI Visualizer | API",
                                            })

def about(request):
    return render_to_response('about.html',{
                                            'title': "CPI Visualizer | About",
                                            })

        

def data(request):
    """
    This function returns json with the data of series to be graphed
    by the Google JS Chart libary.
    required url parameters:
        seriers=series_id,seried_id (various ids can be separated by commas)
    optional url parameters:
        year = number
        year_start = number
        year_end = number
    """
    #Checking parameters
    #required
    series = request.GET.get('series', '')
    if series == '':
        #parameter required
        return HttpResponseBadRequest()
    
    #optional
    year_r = request.GET.get('year', datetime.date.today().year)
    year_start_r = request.GET.get('year_start', '')
    year_end_r = request.GET.get('year_end', '')
    
    #set true if we want a period
    is_period = (year_start_r != '' and year_end_r != '')
    
    #testing the parameters
    try:
        if is_period:
            year_start = int(year_start_r)
            year_end = int(year_end_r)
        else:
            year = int(year_r)
    except ValueError:
        #TODO: add descriptive error
        error = dict(status=400, 
                         message='Parameter should be numbers')
        return HttpResponseBadRequest(simplejson.dumps(error),
                                        content_type='application/json')
    
    series_id = series.split(',')
    
    for entry in series_id:
        try:
            series = Series.objects.get(pk=entry)
            
            #checking if there's data for the selected years
            if is_period:
                if (year_start < series.begin_year.year 
                    and year_end > series.end_year.year):
                    #TODO: Add a descriptive error
                    error = dict(status=400, 
                         message='There is no data for that range of years, range is from %d to %d' % (series.begin_year.year, series.end_year.year)
                         )
                    return HttpResponseBadRequest(simplejson.dumps(error),
                                        content_type='application/json')
            else:
                if (year < series.begin_year.year
                    or year > series.end_year.year):
                    error = dict(status=400, 
                         message='There is no data for that range of years, range is from %d to %d' % (series.begin_year.year, series.end_year.year)
                         )
                    return HttpResponseBadRequest(simplejson.dumps(error),
                                        content_type='application/json')
                     
        except Series.DoesNotExist:
            error = dict(status=404, 
                         message='Series %s does not exist' % entry)
            return HttpResponseNotFound(simplejson.dumps(error),
                                        content_type='application/json')
    
    length = len(series_id)
    all_data = {}
    for entry in series_id:
        #Getting the data we need
        qs = Observation.objects.filter(series__id= entry, date__day = 1)
        if is_period:
            qs = qs.filter(date__gte = datetime.date(year_start, 1, 1), 
                           date__lte = datetime.date(year_end, 12, 31 ) )
        else:
            qs = qs.filter(date__year = year )
            
        if qs.exists():
            for observation in qs:
                date = observation.date
                if date in all_data:
                    all_data[date][entry] = observation.value
                else:
                    if length > 1:
                        #initializing the dictionay
                        all_data[date] = {entry: observation.value}
                    else:
                        #have to deal with the case when user asks for only one series
                        all_data[date] = observation.value
        else:
            #what to do if the qs does not exist? 
            #TODO: ?
            pass
    
    temp = {}
    if length > 1:
        for entry in series_id:
            temp[entry] = 'number'
    else:
        #schema for only one series
        temp = (series_id[0], 'number')
    
    description = {
                   ('date','date'): temp
                   }
    
    order = series_id.insert(0, 'date')
    data_table = gviz_api.DataTable(description)
    data_table.LoadData(all_data)
    json = data_table.ToJSon(columns_order=order , order_by='date')
    
    return HttpResponse(json, 'application/json')

#TODO: remove this after done testing
def test(request):
    return HttpResponse('test')
