'''
Created on Nov 16, 2012

@author: aldo
'''

from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.resources import ALL_WITH_RELATIONS, ALL
from cpi_visualizer.models import Observation, Series, Area, Item

class BaseResource(ModelResource):
    class Meta:
        allowed_methods = ['get']

    def dehydrate(self, bundle):

        try:
            bundle.data['parent'] = self.get_resource_uri(bundle.obj.get_parent())
        except AttributeError:
            bundle.data['parent'] = None

        try:
            bundle.data['children'] = [self.get_resource_uri(child) for child in bundle.obj.get_children()]
        except AttributeError:
            bundle.data['children'] = None

        try:
            bundle.data['siblings'] = [self.get_resource_uri(child) for child in bundle.obj.get_siblings()]
        except AttributeError:
            bundle.data['siblings'] = None

        return bundle

class ItemResource(BaseResource):
    areas_in_mutual_series = fields.ToManyField('cpi_visualizer.api.AreaResource', attribute=lambda bundle: Area.objects.filter(series__item__id__exact=bundle.obj.id).distinct())

    class Meta:
        queryset = Item.objects.all()
        excludes = ['depth', 'numchild', 'path', ]
        filtering = {
                     "name": ('icontains'),
                     "id": ALL,
                     }
    
class AreaResource(BaseResource):
    items_in_mutual_series = fields.ToManyField(ItemResource, attribute=lambda bundle: Item.objects.filter(series__area__id__exact=bundle.obj.id).distinct())

    class Meta:
        queryset = Area.objects.all()
        excludes = ['depth', 'numchild', 'path', ]
        filtering = {
                     "name": ('icontains'),
                     "id": ALL,
                     }


class SeriesResource(BaseResource):
    area = fields.ForeignKey(AreaResource, 'area', full=False)
    item = fields.ForeignKey(ItemResource, 'item', full=False)

    class Meta:
        queryset = Series.objects.all()
        filtering = {
                     "id": ('exact'),
                     "name": ('icontains'),
                     "area": ALL_WITH_RELATIONS,
                     "item": ALL_WITH_RELATIONS
                     }

    def dehydrate(self, bundle):
        # don't add parent/child information because it does not apply
        return bundle

class ObservationResource(BaseResource):
    series = fields.ForeignKey(SeriesResource, 'series', full=False)

    class Meta:
        queryset = Observation.objects.all()
        filtering = {
                     "date": ('gt', 'lt', 'year', 'day'),
                     "series": ALL_WITH_RELATIONS,
                     }

    def dehydrate(self, bundle):
        # don't add parent/child information because it does not apply
        return bundle