from treebeard.mp_tree import MP_Node
from django.db import models

class Item(MP_Node):
    id = models.CharField(primary_key=True, max_length=20)
    name = models.CharField(max_length=256)

    def __unicode__(self):
        return self.name

    class Meta:
            db_table = 'item'

class Area(MP_Node):
    id = models.CharField(primary_key=True, max_length=20)
    name = models.CharField(max_length=256)

    def __unicode__(self):
            return self.name

    class Meta:
            db_table = 'area'

class Series(models.Model):
    SEASONALITY_CHOICES = (
        ('S', 'Seasonally-Adjusted'),
        ('U', 'Unadjusted')
    )

    PERIODICITY_CHOICES = (
        ('R', 'Monthly'),
        ('S', 'Semi-Annual')
    )

    SURVERY_CHOICES = (
        ('CU', 'All Urban Consumers'),
        ('CW', 'Urban Wage Earners & Clerical Workers'),
        ('SU', 'All Urban Consumers - Chained CPI'),
        ('AP', 'Average Price Data'),
        ('LI', 'Department Store Inventory Price Index'),
    )

    id = models.CharField(primary_key=True, max_length=20)
    area = models.ForeignKey(Area)
    item = models.ForeignKey(Item)
    seasonality = models.CharField(max_length=1, choices=SEASONALITY_CHOICES)
    periodicity = models.CharField(max_length=1, choices=PERIODICITY_CHOICES)
    base_period = models.DateField()
    begin_year = models.DateField()
    end_year = models.DateField()
    survey_type = models.CharField(max_length=4, choices=SURVERY_CHOICES)

    def __unicode__(self):
        return self.id

    class Meta:
        db_table = 'series'


class Observation(models.Model):
    series = models.ForeignKey(Series)
    date = models.DateField()
    value = models.FloatField()

    def __unicode__(self):
        return '%s - %s - %s' % (self.series, self.date, self.value)

    class Meta:
            db_table = 'observation'
            unique_together = ('series', 'date')
