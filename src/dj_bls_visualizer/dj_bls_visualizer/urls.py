from django.conf.urls import patterns, include, url
from tastypie.api import Api
from cpi_visualizer.api import ObservationResource, SeriesResource, ItemResource, AreaResource
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(ObservationResource())
v1_api.register(SeriesResource())
v1_api.register(ItemResource())
v1_api.register(AreaResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dj_bls_visualizer.views.home', name='home'),
    # url(r'^dj_bls_visualizer/', include('dj_bls_visualizer.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    (r'^$', 'cpi_visualizer.views.home'),
    (r'^about/$','cpi_visualizer.views.about'),
    (r'^api/$','cpi_visualizer.views.api'),
    (r'^data/$','cpi_visualizer.views.data'),
    (r'^api/', include(v1_api.urls)),
    #TODO: remove after testing
    #views used for testing
    (r'^test/','cpi_visualizer.views.test'),
)
