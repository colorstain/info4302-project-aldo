var HOST = 'http://localhost:8000'
var resources = ['area', 'item']
var selectors = {}
var objects = {}
var id_regex = /.+\/(.+)\/$/
var original_filtering_selector = false
var series_ids = false

initialize_selectors()


function get_id_from_url(url)
{
    return url.match(id_regex)[1]
}

function update_series_ids()
{
    var area = $('#area').val();
    var item = $('#item').val();

    if (area != '' && item != '')
    {
        var mutual_series

        $.ajax({
          url: sprintf(HOST+ "/api/v1/series/?format=json&limit=0&area__id=%s&item__id=%s", area, item),
          async: false,
          dataType: 'json',
          success: function (json) {
            mutual_series = json.objects
          }
        });

        series_ids = []
        $(mutual_series).each(function(){
            series_ids.push(this.id)
        });
    }
}

function reset_selectors()
{
    for (i in resources)
    {
        var resource_name = resources[i]

        $('#'+resource_name).empty();
        $('#'+resource_name).append($("<option>").attr('value','').text(''));

        for (object in objects[resource_name])
        {
            $('#'+resource_name).append($("<option></option>")
                     .attr("value", objects[resource_name][object].id).text(objects[resource_name][object].name));
        }

        $('#'+resource_name).trigger("liszt:updated");
    }

    series_ids = false;
    original_filtering_selector = false;
}

function update_other_selector(selector_id, selector_value)
{
    // only update the other selector if there hasn't already been a filtering.
    if ( original_filtering_selector && selector_id != original_filtering_selector) return;

    var complement = selector_id == 'area' ? 'item' : 'area' ;

    complement_objects = []

    $(objects[selector_id][selector_value][complement+'s_in_mutual_series']).each( function() {
        complement_id = get_id_from_url(this)
        complement_objects.push({id: complement_id, name: objects[complement][complement_id].name})
    });

    $('#'+complement).empty();
    $('#'+complement).append($("<option>").attr('value','').text(''));
    $(complement_objects).each(function() {
        $('#'+complement).append($("<option></option>")
         .attr("value", this.id).text(this.name));
    });
    $('#'+complement).trigger("liszt:updated");

    original_filtering_selector = selector_id
    series_ids = false;
}


function create_chosen_selector(resource_name, data, parent_element)
{
    var select = $('<select>').appendTo('#series-select');
    select.attr('data-placeholder', sprintf('choose an %s...', resource_name));
    select.attr('id', resource_name);

    //create an empty item for placeholder text
    select.append($("<option>").attr('value','').text(''));

    // cache areas and items
    objects[resource_name] = {}

    $(data).each( function() {
        objects[resource_name][this.id] = this

        select.append($("<option>").attr('value',this.id).text(this.name));
    });

    select.chosen({no_results_text: sprintf("no %ss matched your search", resource_name)});

    selectors[resource_name+'_select'] = select;

    select.bind('change', function(){
            update_other_selector($(this).attr('id'), $(this).val());
            update_series_ids();
       });
}

function initialize_selectors()
{
    for (i in resources)
    {
        resource_name = resources[i]

        $.ajax({
          url: sprintf(HOST + "/api/v1/%s/?format=json&limit=0", resource_name),
          async: false,
          dataType: 'json',
          success: function (json) {
            resource_data = json.objects
          }
        });

        create_chosen_selector(resource_name, resource_data)
    }
}

