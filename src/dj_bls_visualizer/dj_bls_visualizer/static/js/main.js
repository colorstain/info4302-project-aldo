var selected_series = false
var series_names = false

google.load("visualization", "1", {packages:["corechart"]});
var params = {
	series:"CUSR0000SA0",
	year:2005
};
google.setOnLoadCallback(getResponse(params));

$("#reset").click(
	function(event) {
		event.preventDefault();
		reset_selectors();
	}
)

$("#reset-list").click(
	function(event) {
		event.preventDefault();
		resetList();
	}
)

$("#graph").click(
	function(event) {
		event.preventDefault();
		drawGraph();
	}
)

$("#add").click(
	function(event) {
		event.preventDefault();
		selectSeries();
	}
)

function getResponse(parameters){
       $.getJSON( '/data/', parameters,
       		function(data) {
        	   var options = {
        	   	title: 'Consumer Product Index',
        	   	legend: {position:'bottom'}
        	   };
        	   var chart_data = new google.visualization.DataTable(data);
        	   var chart = new google.visualization.LineChart(
        	   	document.getElementById("data"));
        	   chart.draw(chart_data, options)
			}
       );
}



function drawGraph() {
	updateSeries();
	if (selected_series) {
		
		var series = selected_series.join(",");
		var year_s = $("#year-start").val();
		var year_e = $("#year-end").val();	
		if (year_s == year_e) {
			var params = {
				series: series,
				year: year_s
			};	
		}
		else {
			var params = {
				series: series,
				year_start: year_s,
				year_end: year_e,
			};
		}
		getResponse(params);
	}
	else {
		alert("Please select some series");
	}
}

function selectSeries() {
	if (series_ids) {
		for (var i = 0; i < series_ids.length; i++) {
			$("#series-list").append($("<label></label>")
				.attr("id", series_ids[i])
				.prepend($("<input>")
					.attr("type", "checkbox")
					.attr("value",series_ids[i])
				)
			);
			seriesLabel(series_ids[i]);
		}
		$("#reset-list").removeClass("hide");
	} else {
		alert("Please select some series")
	}
}

function updateSeries() {
	var selected = [];
	$("#series-list :checked").each( function() {
			selected.push($(this).val());
		}
	)
	if (selected.length >= 1) {
		selected_series = selected;
	}
}

function resetList() {
	selected_series = false;
	series_names = false;
	$("#series-list").empty();
	$("#reset-list").addClass("hide");
}

function seriesLabel(series_id) {
	var uri = "/api/v1/series/" + series_id;
	$.getJSON(uri, function(data) {
				$.when($.getJSON(data.area), $.getJSON(data.item)).done(function(area_data, item_data) {
					$("#"+series_id).append(area_data[0].name + " - " + item_data[0].name + " Periodicity:"  + data.periodicity + " Seasonality:" + data.seasonality);
					}
				);
		}
	);
}


